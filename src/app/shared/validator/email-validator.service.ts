import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';

//Delay es un pipe que permite poder esperar o hacer que algo demore antes de emitir el siguiente valod

@Injectable({
  providedIn: 'root'
})
export class EmailValidatorService implements AsyncValidator {

  constructor(private http: HttpClient) { }
  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    
    const email = control.value;
    console.log(email)

    return this.http.get<any[]>(`http://localhost:3000/usuarios?q=${email}`)
          .pipe(
            // delay(3000), 
            map(resp => {
              //Si regresa un arreglo vacio es que el correo no ha sido tomado
              // Si regresa un arreglo con info el correo existe
              return (resp.length)
                ? null
                : {emailTomado: true}
            })
          )
  }
}
