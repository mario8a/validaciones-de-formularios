import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {

  // miFormulario: FormGroup = new FormGroup({
  //   nombre: new FormControl('RTX 4022to'),
  //   precio: new FormControl(0),
  //   existencias: new FormControl(5)
  // })

  miFormulario: FormGroup = this.fb.group({
    //* [valor inicial, validadores sincronos, valida asincronos]
    nombre: [ , [Validators.required, Validators.minLength(3)]],
    precio: [ , [Validators.required, Validators.min(0)]],
    existencias: [ , [Validators.required, Validators.min(0)]]
  })

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.miFormulario.reset({
      nombre: 'RT54 200',
      precio: 1600,
      existencias: 1
    })
    
  }

  campoEsValido(campo: string) {
    return this.miFormulario.controls[campo].errors 
          && this.miFormulario.controls[campo].touched
  }

  guardar() {

    if(!this.miFormulario.valid) {
      this.miFormulario.markAllAsTouched()
      return;
    }

    console.log(this.miFormulario.value)
    this.miFormulario.reset();
  }

}
