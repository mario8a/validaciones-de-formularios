import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {

  //Elemento a buscar por su referencia local
  @ViewChild('miFormulario') miFormulario!: NgForm;

  //Valores iniciales del formulario
  initForm = {
    producto: 'RTC 5000',
    precio: 10,
    existencia: 10
  }

  constructor() { console.log(this.miFormulario) }

  ngOnInit(): void {
  }

  nombreValido(): boolean {

    return this.miFormulario?.controls.producto?.invalid &&
            this.miFormulario?.controls.producto?.touched

  }

  precioValido() {
    return this.miFormulario?.controls.precio?.touched && 
     this.miFormulario?.controls.precio?.value < 0
  }

  guardar() {
    // console.log(this.miFormulario);
    console.log('Posteo correctp');

    //Resetear formulario
    this.miFormulario.resetForm({
        precio: 0,
        existencia: 0
    }); 
  }

}
